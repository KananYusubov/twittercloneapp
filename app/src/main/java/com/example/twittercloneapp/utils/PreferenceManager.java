package com.example.twittercloneapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.example.twittercloneapp.utils.Constants.PREF_FILE_NAME;

public class PreferenceManager {
    private static SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public PreferenceManager(Context context) {
        this.context = context;
        initPreferences();
    }

    private void initPreferences() {
        sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        editor = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
    }

    private void setStringElement(String key, String val) {
        sharedPreferences.edit().putString(key, val).apply();
    }

    private String getStringElement(String key) {
        return sharedPreferences.getString(key, null);
    }

    private void setBooleanElement(String key, boolean val) {
        sharedPreferences.edit().putBoolean(key, val).apply();
    }

    private boolean getBooleanElement(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    private void setIntElement(String key, int val) {
        sharedPreferences.edit().putInt(key, val).apply();
    }

    private int getIntElement(String key) {
        return sharedPreferences.getInt(key, -1);
    }


    private void setLongElement(String key, long val) {
        sharedPreferences.edit().putLong(key, val).apply();
    }

    private long getLongElement(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public void clearAll() {
        editor.clear();
        editor.apply();
    }

    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }
}
