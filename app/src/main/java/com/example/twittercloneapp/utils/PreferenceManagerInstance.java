package com.example.twittercloneapp.utils;

import android.content.Context;

public class PreferenceManagerInstance {
    private static PreferenceManager INSTANCE;

    private PreferenceManagerInstance() {
    }

    public static PreferenceManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new PreferenceManager(context);
        }
        return INSTANCE;
    }
}
