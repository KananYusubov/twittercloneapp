package com.example.twittercloneapp.ui.activities.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.twittercloneapp.R;
import com.example.twittercloneapp.ui.activities.welcome.WelcomeActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(() -> {
            /* Create an Intent that will start the Menu-Activity. */
            Intent welcomeIntent = new Intent(SplashActivity.this, WelcomeActivity.class);
            startActivity(welcomeIntent);
            finish();
        }, 1500);
    }
}
