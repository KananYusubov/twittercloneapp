package com.example.twittercloneapp.ui;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
