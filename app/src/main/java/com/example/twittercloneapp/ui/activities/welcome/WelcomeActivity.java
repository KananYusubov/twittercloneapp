package com.example.twittercloneapp.ui.activities.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.twittercloneapp.R;
import com.example.twittercloneapp.ui.activities.create_account.CreateAccountActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_create_account)
    void onClickCreateAccount() {
        Intent createAccountIntent = new Intent(WelcomeActivity.this, CreateAccountActivity.class);
        startActivity(createAccountIntent);
    }

    @OnClick(R.id.text_log_in)
    void onClickTextLogIn() {
        Intent logInItent = new Intent(WelcomeActivity.this, CreateAccountActivity.class);
        startActivity(logInItent);
    }
}
